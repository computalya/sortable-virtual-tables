User.create([
  { name: 'Luke', lastname: 'Skywalker', email: 'luke@caglar.com' },
  { name: 'Anakin', lastname: 'Skywalker', email: 'anakin@gmail.com' },
  { name: 'Leia', lastname: 'Organa', email: 'leia@organa.com' },
  { name: 'Darth', lastname: 'Vader', email: 'vader@hotmail.com' }
])
