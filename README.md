# README

## screenshot

![sortable-virtual-tables](./sortable-virtual-tables.gif)


## create a demoapp

```bash
rails new sortable-virtual-tables -B -T
cd sortable-virtual-tables

bundle install --local

rails g scaffold User name lastname email
rails db:migrate
```

> config/routes.rb

```ruby
Rails.application.routes.draw do
  resources :users

  root 'users#index'
end
```

> db/seeds.rb

```ruby
User.create([
  { name: 'Luke', lastname: 'Skywalker', email: 'luke@caglar.com' },
  { name: 'Anakin', lastname: 'Skywalker', email: 'anakin@gmail.com' },
  { name: 'Leia', lastname: 'Organa', email: 'leia@organa.com' },
  { name: 'Darth', lastname: 'Vader', email: 'vader@hotmail.com' }
])
```

`rails db:seed`

## make columns sortable

just sort through controller

> app/view/users/index.html.erb

```html
  <thead>
    <tr>
      <th><%= link_to 'Name', sort: 'name' %></th>
      <th><%= link_to 'Lastname', sort: 'lastname' %></th> 
      <th><%= link_to 'Email', sort: 'email' %></th> 
      <th colspan="3"></th>
    </tr>
  </thead>
```

> app/controllers/users_controller.rb

```ruby
  def index
    @users = User.order(params[:sort])
  end
```

## use an array instead of ActiveRecord Relation

> app/controllers/users_controller.rb

```ruby
  def index
    # @users = User.order(params[:sort])
    # read users in to an array
    # add a virtual column which converts name + fullname to ascii code and adds a number
    users = User.all.pluck(:id, :name, :lastname, :email).map do |id, name, lastname, email|
      class_eval { attr_accessor :asciisum }
      { id: id, name: name, lastname: lastname, email: email, asciisum:  (name + " " + lastname).unpack('C*').sum }
    end

    # convert params[:sort] to symbol (i.e. :name)
    @users = users.sort_by!{|u| u[params[:sort].to_sym]}
  end
```

> app/views/users/index.html.erb

```html
<table>
  <thead>
    <tr>
      <th><%= link_to 'Name', sort: 'name' %></th>
      <th><%= link_to 'Lastname', sort: 'lastname' %></th> 
      <th><%= link_to 'Email', sort: 'email' %></th> 
      <th><%= link_to 'Ascii Sum', sort: 'asciisum' %></th> 
      <th colspan="3"></th>
    </tr>
  </thead>

  <tbody>
    <% @users.each do |user| %>
      <tr>
        <td><%= user[:name] %></td>
        <td><%= user[:lastname] %></td>
        <td><%= user[:email] %></td>
        <td><%= user[:asciisum] %></td>
        <td><%= link_to 'Show', user_path(user[:id]) %></td>
        <td><%= link_to 'Edit', edit_user_path(user[:id]) %></td>
        <td><%= link_to 'Destroy', user_path(user[:id]), method: :delete, data: { confirm: 'Are you sure?' } %></td>
      </tr>
    <% end %>
  </tbody>
</table>
```

## use helper method

> app/view/users/index.html.erb

```html
  <thead>
    <tr>
      <th><%= sortable 'name'  %></th>
      <th><%= sortable 'lastname'  %></th> 
      <th><%= sortable 'email' %></th> 
      <th><%= sortable 'asciisum', 'Ascii Sum' %></th> 
      <th colspan="3"></th>
    </tr>
  </thead>
```

> app/helpers/users_helper.rb

```ruby
module UsersHelper
  def sortable(column, title = nil)
    title ||= column.titleize
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"

    link_to title, sort: column, direction: direction
  end
end
```

> app/controllers/users_controller.rb

```ruby
  helper_method :sort_column, :sort_direction, :sort_array
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
    # read users in to an array
    @users = User.all.pluck(:id, :name, :lastname, :email).map do |id, name, lastname, email|
      class_eval { attr_accessor :asciisum }
      { id: id, name: name, lastname: lastname, email: email, asciisum:  (name + " " + lastname).unpack('C*').sum }
    end

    @users = sort_array(@users, sort_column, sort_direction)
  end

  private
    def sort_column
      # allow only available column names in params[:sort]
      params[:sort] = "name" unless params[:sort].present?
      @users.first.keys.include?(params[:sort].to_sym) ? params[:sort] : "name"
    end

    def sort_direction
      # allow only [asc desc] in params[:direction]
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end

    def sort_array(array, column, direction)
      column = column.to_sym

      direction == "asc" ? array.sort_by{|u| u[column]} : array.sort_by{|u| u[column]}.reverse
    end
```

## add arrows

> app/helpers/users_helper.rb

```ruby
module UsersHelper
  def sortable(column, title = nil)
    title ||= column.titleize

    # set arrow ASCII codes
      if column == sort_column
        arrow = sort_direction == 'asc' ? "&Darr;" : "&Uarr;"
      end
      
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"

    link_to "#{title}#{arrow}".html_safe, sort: column, direction: direction
  end
end
```

